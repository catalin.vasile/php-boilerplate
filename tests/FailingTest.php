<?php
/**
 * Example test. Remove this before adding your source code.
 */

declare(strict_types = 1);

use PHPUnit\Framework\TestCase;

class FailingTest extends TestCase
{
    /** @test */
    function failing_test()
    {
        $this->assertTrue(false);
    }
}
