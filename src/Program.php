<?php

declare(strict_types = 1);

namespace App;

class Program
{
  public function sayHello(): void
  {
    echo 'Hello!';
  }
}