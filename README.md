<p align="center">
  <a href="" rel="noopener">
</p>

<h3 align="center">php-boilerplate</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center"> A containerized development environment for PHP 7.4 with XDebug enabled.
    <br> 
</p>

## 📝 Table of Contents

- [Project Structure](#project_structure)
- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Built Using](#built_using)
- [TODO](../TODO.md)
- [Authors](#authors)

## 📁 Project Structure <a name = "project_structure"></a>

```
php-boilerplate
├─ docker
│  └─ app
│     ├─ nginx
│     │  ├─ default.conf
│     │  └─ logging.conf
│     └─ php
│        ├─ .DS_Store
│        └─ www.conf
├─ src
│  └─ Program.php
├─ tests
│  └─ FailingTest.php
├─ index.php
├─ Dockerfile
├─ docker-compose.yml
├─ composer.json
├─ phpunit.xml
├─ .env.example
├─ .gitignore
├─ LICENSE
├─ README.md
└─ TODO.md
```  

## 🧐 About <a name = "about"></a>

Rather than having multiple dependencies running on your system in order to run PHP (or any other language), one would want to develop on a clean environment, independent of the system environment and the dependencies already installed.

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [usage](#usage) for basic commands.

### Prerequisites

```
Docker; docker-compose;
```

### Installing

In order for docker-compose to be able to read the required variables run the following command in your terminal.

```
cp .env.example .env
```  
`.env` contains configurable variables which are used to start the application with docker-compose.

Development source code should be added in `src/` and tests in `tests/`.


## 🔧 Running the tests <a name = "tests"></a>

`docker-compose run phpunit`

## 🎈 Local Development Usage <a name="usage"></a>

Running **php-fpm** and **nginx**:  
`docker-compose up php-app nginx`

Running **composer** command example:  
`docker-compose run composer require <package>`

## ⛏️ Built Using <a name = "built_using"></a>

- [Docker](https://www.docker.com/)
- [Nginx](https://www.nginx.com/)
- [PHP](https://www.php.net/)

## ✍️ Authors <a name = "authors"></a>

- [@catalin.vasile](https://gitlab.com/catalin.vasile)